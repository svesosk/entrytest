package pages;

import org.openqa.selenium.WebElement;

public class SignUpPage extends BasePage{

	private final String UserNameFieldId = "name";
	private final String EmailFieldXpath = "(//input[@id='email'])[2]";
	private final String PasswordFieldId = "password";
	private final String PasswordConfirmationFieldId = "password2";
	private final String SignUpButtonXpath = "//*[@value='Sign up']";
		
	public WebElement userNameField() {
		return FindElementById(UserNameFieldId);
	}
	
	public WebElement emailField() {
		return FindElementByXpath(EmailFieldXpath);
	}
	
	public WebElement passwordField() {
		return FindElementById(PasswordFieldId);
	}
	
	public WebElement passwordConfirmationField() {
		return FindElementById(PasswordConfirmationFieldId);
	}
	
	public WebElement SignUpButton() {
		return FindElementByXpath(SignUpButtonXpath);
	}
		
}

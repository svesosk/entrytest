package pages;

import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {
	
	private final String LoginButtonXpath = "//button[@data-uitest='login-button']";
	private final String SignUpButtonXpath = "(//button[@data-casper='user-target-section-register-choice'])[2]";
	private final String SignUpWithEmailAddressButtonXpath = "//button[@data-casper=\"user-target-section-register\"]";
	private final String ProfileButtonXpath = "//a[@data-uitest='profile-avatar']";
	
	public WebElement LoginButton() {
		return FindElementByXpath(LoginButtonXpath);
	}
	
	public WebElement SignUpButton() {
		return FindElementByXpath(SignUpButtonXpath);
	}
	
	public WebElement SignUpWithEmailAddressButton() {
		return FindElementByXpath(SignUpWithEmailAddressButtonXpath);
	}
	
	public WebElement ProfileButton() {
		return FindElementByXpath(ProfileButtonXpath);
	}
}

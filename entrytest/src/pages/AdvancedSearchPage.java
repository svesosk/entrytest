package pages;

import org.openqa.selenium.WebElement;

public class AdvancedSearchPage extends BasePage {

	private final String NameFieldId = "token-input-QueryDescriptor_AdvancedSearchOptions_ArtistCriteria_InvolvedMakerName";
	private final String SingleDropdownResultXpath = "//ul/li/a[@href='#']";
	private final String TitleFieldId = "QueryDescriptor_AdvancedSearchOptions_ObjectCriteria_Title";
	private final String OnlyWithImageCheckboxId = "QueryDescriptor_AdvancedSearchOptions_ObjectCriteria_ShowImage";
	private final String MadeBetweenStartFieldId = "QueryDescriptor_AdvancedSearchOptions_ObjectCriteria_DatingPeriod_StartDate";
	private final String MadeBetweenEndFieldId = "QueryDescriptor_AdvancedSearchOptions_ObjectCriteria_DatingPeriod_EndDate";
	private final String FindButtonXpath = "//input[@value='Find']";
	
	public WebElement NameField() {
		return FindElementById(NameFieldId);
	}
	
	public WebElement SingleDropdownResult() {
		return FindElementByXpath(SingleDropdownResultXpath);
	}
	
	public WebElement TitleField() {
		return FindElementById(TitleFieldId);
	}
	
	public WebElement OnlyWithImageCheckbox() {
		return FindElementById(OnlyWithImageCheckboxId);
	}
	
	public WebElement MadeBetweenStartField() {
		return FindElementById(MadeBetweenStartFieldId);
	}
	
	public WebElement MadeBetweenEndField() {
		return FindElementById(MadeBetweenEndFieldId);
	}
	
	public WebElement FindButton() {
		return FindElementByXpath(FindButtonXpath);
	}
	
}

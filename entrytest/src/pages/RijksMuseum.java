package pages;

public class RijksMuseum{

	public RijksMuseum() {
		_homePage = new HomePage();
		_signUpPage = new SignUpPage();
		_profilePage = new ProfilePage();
		_rijksStudioPage = new RijksStudioPage();
		_johannesVermeerPage = new JohannesVermeerPage();
		_myCollectionPage = new MyCollectionPage();
		_searchPage = new SearchPage();
		_advancedSearchPage = new AdvancedSearchPage();
	}
	
	private HomePage _homePage;
	private SignUpPage _signUpPage;
	private ProfilePage _profilePage;
	private RijksStudioPage _rijksStudioPage;
	private JohannesVermeerPage _johannesVermeerPage;
	private MyCollectionPage _myCollectionPage;
	private SearchPage _searchPage;
	private AdvancedSearchPage _advancedSearchPage;
	
	public HomePage HomePage() {
		return _homePage;
	}
	
	public SignUpPage SignUpPage() {
		return _signUpPage;
	}
	
	public ProfilePage ProfilePage() {
		return _profilePage;
	}
	
	public RijksStudioPage RijksStudioPage() {
		return _rijksStudioPage;
	}
	
	public JohannesVermeerPage JohannesVermeerPage() {
		return _johannesVermeerPage;
	}
	
	public MyCollectionPage MyCollectionPage() {
		return _myCollectionPage;
	}
	
	public SearchPage SearchPage() {
		return _searchPage;
	}
	
	public AdvancedSearchPage AdvancedSearchPage() {
		return _advancedSearchPage;
	}
}

package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import testEnvironmentConfiguration.TestEnvironmentConfiguration;

public class BasePage {

	private WebDriver _driver = TestEnvironmentConfiguration.getDriver("chrome");
	
	private WebElement FindElement(By by) {
		WebDriverWait wait = new WebDriverWait(_driver, 5);
		
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
	private List<WebElement> FindElements(By by){
		return _driver.findElements(by);
	}
	
	protected WebElement FindElementByXpath(String xpath) {
		return FindElement(By.xpath(xpath));
	}
	
	protected WebElement FindElementById(String id) {
		return FindElement(By.id(id));
	}
	
	protected WebElement FindElementByLinkText(String linkText) {
		return FindElement(By.linkText(linkText));
	}
	
	protected List<WebElement> FindElementsByXpath(String xpath){
		return FindElements(By.xpath(xpath));
	}

	protected List<WebElement> FindElementsByXpath(String xpath2){
		return FindElements(By.xpath(xpath2));
	}

	protected List<WebElement> FindElementsByXpath(String xpath3){
		return FindElements(By.xpath(xpath3));
	}
}

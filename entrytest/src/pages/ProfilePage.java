package pages;

import org.openqa.selenium.WebElement;

public class ProfilePage extends BasePage {

	private final String ExploreRijksstudioButtonXpath = "//nav[@class='buttons-inline']/a[@href='/en/rijksstudio']"; 
	private final String SearchRijksstudioButtonXpath = "//nav[@class='buttons-inline']/a[@href='/en/search']";
	
	public WebElement ExploreRijksstudioButton() {
		return FindElementByXpath(ExploreRijksstudioButtonXpath);
	}
	
	public WebElement SearchRijksstudioButtonXpath() {
		return FindElementByXpath(SearchRijksstudioButtonXpath);
	}
}

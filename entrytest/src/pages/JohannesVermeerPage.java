package pages;

import org.openqa.selenium.WebElement;

public class JohannesVermeerPage extends BasePage {

	private final String TheMilkmaidPaintingHeartButtonXpath = "//*[@data-title='The Milkmaid']";
	private final String TheMilkmaidPaintingImage = "(//a[@href=\"/en/collection/SK-A-2344\"])[1]";
	private final String MyFirstCollectionButtonXpath = "//*[@data-set-name='My first collection']";
	private final String OverlayCloseButtonXpath = "//div[@data-role='lightbox-first-time-added']/button[@data-role='lightbox-close']";
	private final String PopupBubbleLinkXpath = "//p[@data-role='feedback-balloon']/a";
	
	public WebElement TheMilkmaidPaintingImage() {
		return FindElementByXpath(TheMilkmaidPaintingImage);
	}
	
	public WebElement TheMilkmaidPaintingHeartButton() {
		return FindElementByXpath(TheMilkmaidPaintingHeartButtonXpath);
	}
	
	public WebElement MyFirstCollectionButton() {
		return FindElementByXpath(MyFirstCollectionButtonXpath);
	}
	
	public WebElement OverlayCloseButton() {
		return FindElementByXpath(OverlayCloseButtonXpath);
	}
	
	public WebElement PopupBubbleLink() {
		return FindElementByXpath(PopupBubbleLinkXpath);
	}
	
}

package pages;

import org.openqa.selenium.WebElement;

public class RijksStudioPage extends BasePage{

	private final String JohannesVermeerLinkXpath = "(//a[@href=\"/en/rijksstudio/artists/johannes-vermeer\"])[1]"; 
	
	public WebElement JohannesVermeerLink() {
		return FindElementByXpath(JohannesVermeerLinkXpath);
	}
}

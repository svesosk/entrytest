package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

public class MyCollectionPage extends BasePage{

	private String PictureXpath = "//figure";
	
	public List<WebElement> Pictures(){
		return FindElementsByXpath(PictureXpath);
	}
	
}

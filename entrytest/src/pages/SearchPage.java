package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

public class SearchPage extends BasePage {

	private final String AdvancedSearchLinkText = "Advanced search";
	private final String PictureXpath = "//figure";
	
	public WebElement AdvancedSearchLink() {
		return FindElementByLinkText(AdvancedSearchLinkText);
	}
	
	public List<WebElement> Pictures() {
		return FindElementsByXpath(PictureXpath);
	}
	
}

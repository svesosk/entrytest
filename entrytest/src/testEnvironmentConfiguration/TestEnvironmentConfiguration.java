package testEnvironmentConfiguration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;

public class TestEnvironmentConfiguration {
	
	private static WebDriver _driver;
	
	private static String _basePath = "bin\\\\testEnvironmentConfiguration\\\\";
	
	public static WebDriver getDriver(String browserType){
		
		if (_driver != null) return _driver;
		
		if(browserType.equalsIgnoreCase("chrome")) 
		{
			System.setProperty("webdriver.chrome.driver", _basePath + "chromedriver.exe"); 
			_driver =  new ChromeDriver();
		}
		else if(browserType.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", _basePath + "geckodriver.exe");
			_driver =  new FirefoxDriver();
		}
		else
		{
			System.setProperty("webdriver.ie.driver", _basePath + "IEDriverServer.exe");
			_driver =  new InternetExplorerDriver();
		}
		
		return _driver;
	}
	
	
	
	public static void NavigateToUrl(String url) {
		_driver.get(url);
	}
	
	public static void HoverOverElement(WebElement element) {
		Actions action = new Actions(_driver);
		action.moveToElement(element).build().perform();
	}
	
	public static void BrowserQuit() {
		_driver.quit();
		_driver = null;
	}
	
	
}

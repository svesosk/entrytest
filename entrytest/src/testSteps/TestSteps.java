package testSteps;

import java.util.UUID;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pages.RijksMuseum;
import testEnvironmentConfiguration.TestEnvironmentConfiguration;

public class TestSteps {

	
	private RijksMuseum _rijksMuseum;
	
	@Before
	public void SetUp() {
		_rijksMuseum = new RijksMuseum();
	}
	
	@After
	public void TearDown() {
		TestEnvironmentConfiguration.BrowserQuit();
	}
	
	@Test
	public void AddPictureToMyCollection() {
		TestEnvironmentConfiguration.NavigateToUrl("https://www.rijksmuseum.nl/en");
		_rijksMuseum.HomePage().LoginButton().click();
		_rijksMuseum.HomePage().SignUpButton().click();
		_rijksMuseum.HomePage().SignUpWithEmailAddressButton().click();
		_rijksMuseum.SignUpPage().userNameField().sendKeys("qweqwe");		
		_rijksMuseum.SignUpPage().emailField().sendKeys("qweqwe" + UUID.randomUUID() + "@qweqwe.com");
		_rijksMuseum.SignUpPage().passwordField().sendKeys("qweqwe");
		_rijksMuseum.SignUpPage().passwordConfirmationField().sendKeys("qweqwe");
		_rijksMuseum.SignUpPage().SignUpButton().click();
		_rijksMuseum.HomePage().ProfileButton().click();
		_rijksMuseum.ProfilePage().ExploreRijksstudioButton().click();
		_rijksMuseum.RijksStudioPage().JohannesVermeerLink().click();
		TestEnvironmentConfiguration.HoverOverElement(_rijksMuseum.JohannesVermeerPage().TheMilkmaidPaintingImage());
		_rijksMuseum.JohannesVermeerPage().TheMilkmaidPaintingHeartButton().click();
		_rijksMuseum.JohannesVermeerPage().MyFirstCollectionButton().click();
		_rijksMuseum.JohannesVermeerPage().OverlayCloseButton().click();
		_rijksMuseum.JohannesVermeerPage().PopupBubbleLink().click();
				
		Assert.assertTrue(_rijksMuseum.MyCollectionPage().Pictures().size() != 0);
	}
	
	@Test
	public void SearchForASpecificPicture() {
		TestEnvironmentConfiguration.NavigateToUrl("https://www.rijksmuseum.nl/en/search");
		_rijksMuseum.SearchPage().AdvancedSearchLink().click();
		
		//"Utagawa Kuniyoshi as the search term does not return any hits so I decided to use only the last name"
		_rijksMuseum.AdvancedSearchPage().NameField().sendKeys("Kuniyoshi");
		
		_rijksMuseum.AdvancedSearchPage().SingleDropdownResult().click();
		_rijksMuseum.AdvancedSearchPage().TitleField().sendKeys("De Chofu Tama");
		_rijksMuseum.AdvancedSearchPage().OnlyWithImageCheckbox().click();
		_rijksMuseum.AdvancedSearchPage().MadeBetweenStartField().sendKeys("1847");
		_rijksMuseum.AdvancedSearchPage().MadeBetweenEndField().sendKeys("1850");
		_rijksMuseum.AdvancedSearchPage().FindButton().click();
		
		//This test will fail because the advanced search using from-to year filter is not working properly.
		Assert.assertTrue(_rijksMuseum.SearchPage().Pictures().size() == 3);
	}
	
}
